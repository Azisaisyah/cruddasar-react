import React, { Component } from "react";
import NavbarComponents from "./NavbarComponents";
import Tabel from "./Tabel";
import Formulir from "./Formulir";

export default class Crud extends Component {
  constructor(props) {
    super(props);

    this.state = {
      biodata: [],
      nama: "",
      telepon: "",
      alamat: "",
      id: "",
    };
  }

  handleChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  handleSubmit = (event) => {
    event.preventDefault();

    if(this.state.id === ""){
      this.setState({
        biodata: [
          ...this.state.biodata,
          {
            id: this.state.biodata.length + 1,
            nama: this.state.nama,
            telepon: this.state.telepon,
            alamat: this.state.alamat,
          },
        ],
      });
    } else {
      const biodataSelainDipilih = this.state.biodata
      .filter((bio) => bio.id !== this.state.id)
      .map((filterBiodata) => {
        return filterBiodata
      })

      this.setState({
        biodata: [
          ...biodataSelainDipilih,
          {
            id: this.state.biodata.length + 1,
            nama: this.state.nama,
            telepon: this.state.telepon,
            alamat: this.state.alamat,
          },
        ],
      })
    }

    this.setState({
      nama: "",
      telepon: "",
      alamat: "",
      id: "",
    });
  };

  editData = (id) => {
    const biodataDipilih = this.state.biodata
      .filter((bio) => bio.id === id)
      .map((filterBiodata) => {
        return filterBiodata
      })
    
      this.setState({
        nama: biodataDipilih[0].nama,
        telepon: biodataDipilih[0].telepon,
        alamat: biodataDipilih[0].alamat,
        id: biodataDipilih[0].id,
      })
  }

  hapusData = (id) => {
    const biodataBaru = this.state.biodata
      .filter((bio) => bio.id !== id)
      .map((filterBiodata) => {
        return filterBiodata
      })

    this.setState({
      biodata : biodataBaru
    })
  }

  render() {
    return (
      <div>
        <NavbarComponents />
        <div className="container mt-4">
          <Tabel biodata={this.state.biodata} editData={this.editData} hapusData={this.hapusData}/>
          <Formulir
            {...this.state}
            handleChange={this.handleChange}
            handleSubmit={this.handleSubmit}
          />
        </div>
      </div>
    );
  }
}
