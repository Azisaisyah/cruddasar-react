import React from "react";
import { Row, Col, Form, Button } from "react-bootstrap";

const Formulir = ({nama, telepon, alamat, handleChange, handleSubmit, id}) => {
  return (
    <div className="mt-5">
      <Row>
        <Col>
          <hr />
          <h4>{ id ? "Edit Data" : "Tambah Data"}</h4>
          <hr />
        </Col>
      </Row>
      <Row>
        <Col>
          <Form onSubmit={handleSubmit}>
            <Form.Group controlId="nama">
              <Form.Label>Nama</Form.Label>
              <Form.Control
                type="text"
                placeholder="Nama Lengkap"
                name="nama"
                value={nama}
                onChange={(event) => handleChange(event)}
              />
            </Form.Group>
            <Form.Group controlId="telepon">
              <Form.Label>Nomor HP</Form.Label>
              <Form.Control
                type="number"
                placeholder="Nomor HP/Telepon"
                name="telepon"
                value={telepon}
                onChange={(event) => handleChange(event)}
              />
            </Form.Group>
            <Form.Group controlId="alamat">
              <Form.Label>Alamat</Form.Label>
              <Form.Control
                as="textarea"
                rows="3"
                placeholder="Alamat"
                name="alamat"
                value={alamat}
                onChange={(event) => handleChange(event)}
              />
            </Form.Group>

            <Button variant="primary" type="submit">
              Submit
            </Button>
          </Form>
        </Col>
      </Row>
    </div>
  );
};

export default Formulir;
