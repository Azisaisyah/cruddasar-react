import React from "react";
import { Table } from "react-bootstrap";

const Tabel = ({ biodata, editData, hapusData }) => {
  return (
    <Table striped bordered hover>
      <thead>
        <tr>
          <th>#</th>
          <th>Nama Lengkap</th>
          <th>Nomor HP</th>
          <th>Alamat</th>
          <th>Aksi</th>
        </tr>
      </thead>
      <tbody>
        {biodata.map((bio, index) => {
          return (
            <tr key={index}>
              <td>{index+1}</td>
              <td>{bio.nama}</td>
              <td>{bio.telepon}</td>
              <td>{bio.alamat}</td>
              <td>
                <button className="btn btn-warning mr-3" onClick={() => editData(bio.id)}>Edit</button>
                <button className="btn btn-danger" onClick={() => hapusData(bio.id)}>Hapus</button>
              </td>
            </tr>
          );
        })}
      </tbody>
    </Table>
  );
};

export default Tabel;
